class InitialDatabase < ActiveRecord::Migration[5.1]
  def change
    create_table :customers do |t|
      t.string :name
      t.string :surname
      t.string :login
      t.string :password
      t.date   :birtday
      t.string :phone
      t.string :email
      t.string :address
      t.string :postcode

      t.timestamps
    end

    create_table :providers do |t|
      t.string :title
      t.string :phone
      t.string :address
      t.string :contact_name

      t.timestamps
    end

    create_table :products do |t|
      t.text       :description
      t.integer    :price
      t.integer    :weight
      t.string     :product_code
      t.references :provider
      t.json       :characteristics
      t.integer    :quantity

      t.timestamps
    end

    create_table :characteristics do |t|
      t.string :name
      t.text   :description

      t.timestamps
    end

    create_table :product_characteristics do |t|
      t.references :product
      t.references :characteristic
      t.string     :value
      t.string     :type

      t.timestamps
    end

    create_table :transport_companies do |t|
      t.string  :name
      t.integer :base_price
      t.integer :free_delivery

      t.timestamps
    end

    create_table :orders do |t|
      t.references :customer
      t.integer    :state
      t.datetime   :date_start
      t.datetime   :date_finish
      t.integer    :price
      t.references :transport_company

      t.timestamps
    end

    create_table :order_products do |t|
      t.references :order
      t.references :product

      t.timestamps
    end

    create_table :images do |t|
      t.string  :url
      t.references :imageable, polymorphic: true

      t.timestamps
    end
  end
end
